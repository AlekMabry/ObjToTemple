#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <ostream>

struct vec3
{
	float x;
	float y;
	float z;
};

struct i64_3
{
	long long x;
	long long y;
	long long z;
};

struct tri
{
	long long vi1;
	long long vi2;
	long long vi3;
	long long vn1;
	long long vn2;
	long long vn3;
};

bool loadObj(const char * path, std::vector<vec3> &out_vertices, std::vector<vec3> &out_normals, std::vector<tri> &out_tris)
{
	FILE * file = fopen(path, "r");
	if (file == NULL)
	{
		printf("[ERROR] Impossible to open the file! \n");
		return false;
	}

	while (true)
	{
		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break;

		if (strcmp(lineHeader, "v") == 0)
		{
			vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			out_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader,"vn") == 0)
		{
			vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			out_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			std::string vert1, vert2, vert3;
			long verti[3], normi[3];

			int matches = fscanf(file, "%d//%d %d//%d %d//%d\n", &verti[0], &normi[0], &verti[1], &normi[1], &verti[2], &normi[2]);
			if (matches != 6) {
				printf("[ERROR] REEEEEEEEE EXPORT WITH ONLY VERTICES AND NORMALS YOU CIA AGENT!\n");
				return false;
			}
			tri triangle;
			triangle.vi1 = verti[0];
			triangle.vi2 = verti[1];
			triangle.vi1 = verti[2];
			triangle.vn1 = normi[0];
			triangle.vn2 = normi[1];
			triangle.vn3 = normi[2];

			out_tris.push_back(triangle);
		}
	}
	fclose(file);
	return true;
}


int main(int argc, char *argv[])
{
	std::vector<vec3> out_vertices;
	std::vector<vec3> out_normals;
	std::vector<tri> out_tris;

	if (loadObj(argv[1], out_vertices, out_normals, out_tris))
	{
		i64_3 *verts = new i64_3[out_vertices.size()];
		i64_3 *norms = new i64_3[out_normals.size()];
		for (int i = 0; i < out_vertices.size(); i++)
		{
			verts[i].x = (long long)out_vertices[i].x;
			verts[i].y = (long long)out_vertices[i].y;
			verts[i].z = (long long)out_vertices[i].z;
		}
		for (int i = 0; i < out_normals.size(); i++)
		{
			norms[i].x = (long long)out_normals[i].x;
			norms[i].y = (long long)out_normals[i].y;
			norms[i].z = (long long)out_normals[i].z;
		}

		long long vertLen = out_vertices.size();
		long long normLen = out_normals.size();
		long long triLen = out_tris.size();
		std::cout << "Vertices: " << vertLen << "\n";
		std::cout << "Normals: " << normLen << "\n";
		std::cout << "Tris: " << triLen << "\n";

		FILE *out;
		out = fopen(argv[2], "wb");
		fwrite(&vertLen, sizeof(long long), 1, out);
		fwrite(&triLen, sizeof(long long), 1, out);
		fwrite(&normLen, sizeof(long long), 1, out);
		fwrite(&verts[0], vertLen*sizeof(i64_3), 1, out);
		fwrite(&out_tris[0], sizeof(tri), triLen, out);
		fwrite(&norms[0], sizeof(i64_3), normLen, out);
		fclose(out);

		delete[] verts;
		delete[] norms;
	}
	else
	{
		std::cout << "[ERROR] Could not convert obj!\n";
	}
}